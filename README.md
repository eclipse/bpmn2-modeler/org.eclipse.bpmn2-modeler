# BPMN2 Modeler

The Eclipse BPMN2 Modeler is a graphical modeling tool for authoring business processes. The primary goal of BPMN2 Modeler was to provide a graphical workflow editing framework, which can be easily customized for any BPMN 2.0 compliant execution engine.

## New BPMN Modeler Platform - Open-BPMN

This project will be migrated into the new BPMN Modeller Plattform [Open-BPMN](https://www.open-bpmn.org/). This will also include a Eclipse Plugin in the near future. 

<img src="doc/images/vs-code-02.png" width="800" />


## Background

Business Process Model and Notation is an XML language which was proposed by the Object Management Group (OMG), as a notation for describing not only business workflows (a.k.a. "processes") but also higher-level collaborations between internal or external business partners and the choreography of information exchanged between these business partners.

The BPMN language was intended for users at all levels, from the business analysts who create the initial design, to the developers who implement the technical details, and finally, to the business users responsible for managing and monitoring the processes.

BPMN 2.0 has evolved to become a complex specification which tries to be all things to all people involved in the design of business processes. Because of this complexity, writing BPMN XML and visualizing business processes becomes nearly impossible without the use of a graphical tool. BPMN2 Modeler fills this need nicely by providing an intuitive user interface. It interprets the complexities of the BPMN language in the form of drawing canvases, tool palettes, property sheets and other familiar UI elements. 

<img src="doc/images/overview-snapshot.gif" width="800" />


## Extensibility

BPMN 2.0 introduces an extensibility mechanism that allows extending standard BPMN elements with additional attributes. It can be used by modelers and modeling tools to add non-standard elements or Artifacts to satisfy a specific need, such as the unique requirements of a vertical domain, and still have valid BPMN Core.

The goal of the Eclipse BPMN2 Modeler is to not only provide a graphical modeling tool, but also to allow plug-in developers to easily customize the behavior and appearance of the editor for specific BPM workflow engines that use this BPMN 2.0 extensibility mechanism.

Open Source Workflow Engines like jBPM and Imixs-Workflow integrate Eclipse BPMN2 into there tooling platforms and extend the core features of BPMN2 with platform specific functionality. Learn more about this projects on the wiki page Open Source Workflow Engines. If you plan to implement you own BPMN2 extensions and learn more about the basics of Eclipse BPMN2 Runtime extensions, read the Wiki Developer Tutorial.

## Architecture

The BPMN2 Modeler is built on the Eclipse Plug-in Architecture and provides several extension points for customizing the editor's appearance and behavior. The Deveoper Tutorials Wiki pages provide several examples and guidelines for extending the capabilities of Eclipse BPMN2 Modeler.

The foundation of the BPMN2 Modeler is the Eclipse BPMN 2.0 EMF meta model which is part of the Model Development Tools (MDT) project. The model was originally developed by contributing members of the OMG BPMN 2.0 working group and is fully compliant with the spec. 


# Build

To build the plugin from sources run

    $ mvn clean install

All dependencies and repository references are defined in the main pom.xml file.
